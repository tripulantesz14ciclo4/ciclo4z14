package com.example.demociclo4.modelos;

import junit.framework.TestCase;

public class UsuarioTest extends TestCase {

    public void testSetNombres() {
        String nombre = "Juan";
        Usuario modelo = new Usuario();
        modelo.setNombres(nombre);
        String resultadoObtenido = modelo.getNombres();
        String resultadoEsperado = nombre.toUpperCase();

        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    public void testSetApellidos() {

    }
}