package com.example.demociclo4.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demociclo4.R;
import com.example.demociclo4.modelos.Usuario;

import java.util.ArrayList;

public class UsuariosRecyclerViewAdapter extends RecyclerView.Adapter<UsuariosRecyclerViewAdapter.ViewHolderUsuarios> {

    private ArrayList<Usuario> registros;

    public UsuariosRecyclerViewAdapter(ArrayList<Usuario> registros) {
        this.registros = registros;
    }

    @NonNull
    @Override
    public UsuariosRecyclerViewAdapter.ViewHolderUsuarios onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_usuario, null, false);
        return new ViewHolderUsuarios(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull UsuariosRecyclerViewAdapter.ViewHolderUsuarios holder, int position) {
        holder.cargarInformacion(registros.get(position));
    }

    @Override
    public int getItemCount() {
        return registros.size();
    }

    public class ViewHolderUsuarios extends RecyclerView.ViewHolder {

        TextView nombreCompleto;
        TextView direccion;
        TextView usuario;

        public ViewHolderUsuarios(@NonNull View itemView) {
            super(itemView);

            nombreCompleto = itemView.findViewById(R.id.adapterUsuario_nombreCompleto);
            direccion = itemView.findViewById(R.id.adapterUsuario_direccion);
            usuario = itemView.findViewById(R.id.adapterUsuario_email);
        }

        public void cargarInformacion(Usuario registro)
        {
            nombreCompleto.setText(registro.getNombres() + " " + registro.getApellidos());
            direccion.setText(registro.getTipdoc() + "-" + registro.getDocumento());
            usuario.setText(registro.getUsuario());
        }
    }
}
