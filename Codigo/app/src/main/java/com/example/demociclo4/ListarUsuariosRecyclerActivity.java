package com.example.demociclo4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.demociclo4.adapters.UsuariosRecyclerViewAdapter;
import com.example.demociclo4.dao.UsuarioDAO;
import com.example.demociclo4.modelos.Usuario;

import java.util.ArrayList;

public class ListarUsuariosRecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DemoCiclo4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_usuarios_recycler);

        RecyclerView recyclerUsuarios = findViewById(R.id.listarUsuariosRecycler_recycler);

        UsuarioDAO baseDatos = new UsuarioDAO(this);
        ArrayList<Usuario> resultadoObtenido = baseDatos.listar(null);

        UsuariosRecyclerViewAdapter adaptadorUsuario = new UsuariosRecyclerViewAdapter(resultadoObtenido);
        recyclerUsuarios.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerUsuarios.setAdapter(adaptadorUsuario);
    }
}