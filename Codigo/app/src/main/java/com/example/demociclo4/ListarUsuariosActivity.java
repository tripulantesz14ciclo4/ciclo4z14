package com.example.demociclo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.demociclo4.adapters.UsuariosAdapter;
import com.example.demociclo4.dao.UsuarioDAO;
import com.example.demociclo4.modelos.Usuario;

import java.util.ArrayList;

public class ListarUsuariosActivity extends AppCompatActivity {

    private ListView listaUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DemoCiclo4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_usuarios);

        this.listaUsuarios = findViewById(R.id.listarUsuarios_lista);

        UsuarioDAO basedatos = new UsuarioDAO(this);
        ArrayList<Usuario> listado = basedatos.listar(null);
        UsuariosAdapter adaptador = new UsuariosAdapter(this, R.layout.item_adapter_usuario, listado);
        listaUsuarios.setAdapter(adaptador);

    }
}