package com.example.demociclo4.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.demociclo4.R;
import com.example.demociclo4.modelos.Usuario;

import org.w3c.dom.Text;

import java.util.List;

public class UsuariosAdapter extends ArrayAdapter<Usuario> {

    private List<Usuario> listado;
    private Context contexto;
    private int layoutUtilizado;

    public UsuariosAdapter(@NonNull Context contexto, int layoutUtilizado, List<Usuario> listado) {
        super(contexto, layoutUtilizado, listado);
        this.listado = listado;
        this.contexto = contexto;
        this.layoutUtilizado = layoutUtilizado;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent)
    {
        View vista = convertView;

        if(vista==null)
        {
            vista = LayoutInflater.from(this.contexto).inflate(R.layout.item_adapter_usuario, null);

            Usuario registro = this.listado.get(position);

            TextView txvNombreCompleto = vista.findViewById(R.id.adapterUsuario_nombreCompleto);
            TextView txvDireccion = vista.findViewById(R.id.adapterUsuario_direccion);
            TextView txvEmail = vista.findViewById(R.id.adapterUsuario_email);

            txvNombreCompleto.setText(registro.getNombres() + " " + registro.getApellidos());
            txvDireccion.setText(registro.getUsuario());
            txvEmail.setText(registro.getTipdoc() + " " + registro.getDocumento());

        }
        return vista;
    }

}
