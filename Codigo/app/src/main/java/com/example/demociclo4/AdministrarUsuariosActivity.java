package com.example.demociclo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AdministrarUsuariosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DemoCiclo4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrar_usuarios);

        Button btnAgregarUsuario = findViewById(R.id.adminUsuarios_btnAgregar);
        btnAgregarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AgregarUsuarioActivity.class);
                startActivity(intent);
            }
        });

        Button btnBuscarUsuario = findViewById(R.id.adminUsuarios_btnBuscar);
        btnBuscarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), BuscarUsuarioActivity.class);
                startActivity(intent);
            }
        });

        Button btnListar = findViewById(R.id.adminUsuarios_btnListar);
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ListarUsuariosActivity.class);
                startActivity(intent);
            }
        });

        Button btnListarRecycler = findViewById(R.id.adminUsuarios_btnListarRecycler);
        btnListarRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ListarUsuariosRecyclerActivity.class);
                startActivity(intent);
            }
        });
    }
}