package com.example.demociclo4.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

public class MensajeDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog (Bundle estado){
        AlertDialog.Builder mensaje = new AlertDialog.Builder(getActivity());
        mensaje.setTitle("Confirmación");
        mensaje.setMessage("La acción que realizará eliminará todos los registros asociados al restaurante. ¿Realmente desea eliminar el registro?");
        mensaje.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Se ha eliminado el registro correctamente.", Toast.LENGTH_LONG).show();
            }
        });
        mensaje.setNegativeButton("Me arrepentí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Excelente desicion", Toast.LENGTH_LONG).show();
            }
        });
        return mensaje.create();
    }
}
