package com.example.demociclo4.classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteConex extends SQLiteOpenHelper {
    private static final String baseDatos = "dbz09";
    private static final int version = 1;

    public SqliteConex(@Nullable Context context) {
        super(context, baseDatos, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE usuarios (\n" +
                "    id        INTEGER      PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                "    tipdoc    VARCHAR (2)  NOT NULL,\n" +
                "    documento VARCHAR (11) NOT NULL,\n" +
                "    nombres   VARCHAR (50) NOT NULL,\n" +
                "    apellidos VARCHAR (50) NOT NULL,\n" +
                "    usuario   VARCHAR (20) NOT NULL,\n" +
                "    clave     VARCHAR (80) NOT NULL\n" +
                ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
