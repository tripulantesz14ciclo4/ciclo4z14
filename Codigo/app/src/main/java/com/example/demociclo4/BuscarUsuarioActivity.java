package com.example.demociclo4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.demociclo4.dao.UsuarioDAO;
import com.example.demociclo4.modelos.Usuario;

public class BuscarUsuarioActivity extends AppCompatActivity {

    private int id;
    private EditText txtDocumentoBusqueda;
    private EditText txtTipDoc;
    private EditText txtDocumento;
    private EditText txtNombres;
    private EditText txtApellidos;
    private EditText txtUsuario;
    private EditText txtClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DemoCiclo4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_usuario);

        txtDocumentoBusqueda = findViewById(R.id.buscarUsuario_txtDocumentoBusqueda);
        txtTipDoc = findViewById(R.id.buscarUsuario_txtTipDoc);
        txtDocumento = findViewById(R.id.buscarUsuario_txtDocumento);
        txtNombres= findViewById(R.id.buscarUsuario_txtNombres);
        txtApellidos = findViewById(R.id.buscarUsuario_txtApellidos);
        txtUsuario = findViewById(R.id.buscarUsuario_txtUsuario);
        txtClave = findViewById(R.id.buscarUsuario_txtClave);


        ImageButton btnBuscar = findViewById(R.id.buscarUsuario_btnBuscar);
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    limpiarFormulario();
                    UsuarioDAO baseDatos =new UsuarioDAO(v.getContext());
                    Usuario modeloObtenido = baseDatos.listar(txtDocumentoBusqueda.getText().toString()).get(0);
                    if(modeloObtenido!=null)
                        asignarModeloAEditText(modeloObtenido);
                }
                catch (Exception ex)
                {}
            }
        });

        Button btnActualizar = findViewById(R.id.buscarUsuario_btnActualizar);
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario modeloCargado = cargarModelo();
                if(modeloCargado!=null)
                {
                    UsuarioDAO baseDatos = new UsuarioDAO(v.getContext());
                    if(baseDatos.actualizar(modeloCargado)>0) {
                        Toast.makeText(v.getContext(), "Se ha actualizado el registro correctamente.", Toast.LENGTH_LONG);
                        limpiarFormulario();
                    }
                    else
                        Toast.makeText(v.getContext(), "Se ha producido un error al intentar actualizar el registro.", Toast.LENGTH_LONG);
                }
                else
                    Toast.makeText(v.getContext(), "Diligencie los campos vacios", Toast.LENGTH_LONG);
            }
        });

        Button btnEliminar = findViewById(R.id.buscarUsuario_btnEliminar);
        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Usuario modeloCargado = cargarModelo();
                if(modeloCargado!=null)
                {
                    AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                    mensaje.setTitle("Confirmación");
                    mensaje.setMessage("¿Realmente desea eliminar el registro?");
                    mensaje.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UsuarioDAO baseDatos = new UsuarioDAO(v.getContext());
                            if(baseDatos.eliminar(modeloCargado)>0)
                                Toast.makeText(v.getContext(), "Se ha eliminado el registro correctamente.", Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(v.getContext(), "Se ha producido un error al intentar eliminar el registro.", Toast.LENGTH_LONG).show();
                        }
                    });
                    mensaje.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    mensaje.create();
                    mensaje.show();
                }
                else
                    Toast.makeText(v.getContext(), "Diligencie los campos vacios", Toast.LENGTH_LONG);
            }
        });
    }

    private void asignarModeloAEditText(Usuario modelo)
    {
        id = modelo.getId();
        txtTipDoc.setText(modelo.getTipdoc());
        txtDocumento.setText(modelo.getDocumento());
        txtNombres.setText(modelo.getNombres());
        txtApellidos.setText(modelo.getApellidos());
        txtUsuario.setText(modelo.getUsuario());
        txtClave.setText(modelo.getClave());
    }

    private void limpiarFormulario()
    {
        id=0;
        txtTipDoc.setText("");
        txtDocumento.setText("");
        txtNombres.setText("");
        txtApellidos.setText("");
        txtUsuario.setText("");
        txtClave.setText("");
    }

    private Usuario cargarModelo()
    {
        Usuario modelo = null;

        if(id>0) {
            String tipdoc = txtTipDoc.getText().toString();
            String documento = txtDocumento.getText().toString();
            String nombres = txtNombres.getText().toString();
            String apellidos = txtApellidos.getText().toString();
            String usuario = txtUsuario.getText().toString();
            String clave = txtClave.getText().toString();

            if (!tipdoc.isEmpty() && !documento.isEmpty() && !nombres.isEmpty() && !apellidos.isEmpty() && !usuario.isEmpty() && !clave.isEmpty())
                modelo = new Usuario(id, tipdoc, documento, nombres, apellidos, usuario, clave);
        }
        return modelo;
    }

}