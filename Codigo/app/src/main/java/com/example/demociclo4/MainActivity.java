package com.example.demociclo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_DemoCiclo4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle datos = getIntent().getExtras();
        if(datos!=null)
        {
            TextView usuario = findViewById(R.id.main_txtUsuario);
            usuario.setText(datos.getString("usuario"));

        }

        Button btnAcercade = findViewById(R.id.adminUsuarios_btnAgregar);
        btnAcercade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://coronavirus.santander.gov.co/wp-content/uploads/2020/07/1.TIP-USO-CORRECTO-TAPABOCAS.pdf"));
                startActivity(intent);
            }
        });

        Button btnAdministrarUsuarios = findViewById(R.id.adminUsuarios_btnBuscar);
        btnAdministrarUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AdministrarUsuariosActivity.class);
                startActivity(intent);
            }
        });

        ImageButton btnSalir = findViewById(R.id.main_ibtSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

}