package com.example.demociclo4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.demociclo4.dao.UsuarioDAO;
import com.example.demociclo4.dialogs.MensajeDialog;
import com.example.demociclo4.modelos.Usuario;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        SystemClock.sleep(5000);
        setTheme(R.style.Theme_DemoCiclo4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Button btnAcceder = findViewById(R.id.login_btnAcceder);
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Conectamos la caja de texto con el objeto que vamos a usar para obtener el valor de cada una
                EditText usuario = findViewById(R.id.login_txtUsuario);
                EditText clave = findViewById(R.id.login_txtClave);

                //Obtenemos el valor de cada caja de texto
                String valorUsuario = usuario.getText().toString();
                String valorClave = clave.getText().toString();

                //Verificamos que no hayan campos en blanco.
                if(!valorUsuario.isEmpty() && !valorClave.isEmpty()) {
                    Intent intent = new Intent(v.getContext(), MainActivity.class);
                    intent.putExtra("usuario", valorUsuario);
                    startActivity(intent);
                }
                else
                {
/*                    AlertDialog.Builder mensaje = new AlertDialog.Builder(v.getContext());
                    mensaje.setTitle("Campos vacios");
                    mensaje.setMessage("Digite todos los campos requeridos.");
                    mensaje.show();*/

                    Toast.makeText(v.getContext(), "Digite todos los campos requeridos.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageButton btnFacebook = findViewById(R.id.login_ibtFacebook);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MensajeDialog mensaje = new MensajeDialog();
                mensaje.show(getFragmentManager(), null);
            }
        });

        ImageButton btnGoogle = findViewById(R.id.login_ibtGoogle);
        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsuarioDAO usuarioDao = new UsuarioDAO(v.getContext());

                Usuario objeto = new Usuario(2,"CC", "1098647852", "Luz Maria", "Villamizar", "lvillamizar@gmail.com", "159753");
//                usuarioDao.insertar(objeto);

                ArrayList<Usuario> usuariosGuardados = usuarioDao.listar(null);

                usuarioDao.eliminar(objeto);

                usuariosGuardados = usuarioDao.listar(null);

            }
        });
    }
}