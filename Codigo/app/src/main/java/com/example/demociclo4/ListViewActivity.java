package com.example.demociclo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        SystemClock.sleep(5000);
        setTheme(R.style.Theme_DemoCiclo4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        ListView lista = (ListView) findViewById(R.id.listaDias);
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.dias));
        lista.setAdapter(adaptador);
    }
}